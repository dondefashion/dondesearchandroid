# DondeSearch Android SDK Integration Document


## Installing

To add the DondeSearch Android SDK to an Android project, add the following dependency to your module's build.gradle:

```
dependencies {
  implementation 'com.search.donde:dondesearch:1.X.X'
}
```
The latest version of the SDK can be found [HERE](https://bintray.com/dondesearch/maven/dondesearch).

## Usage

### SDK Initialization

To initialize the Donde SDK, get your API key and App ID and add the following lines to your app:
```
private static final String DONDE_API_KEY = "YOUR_DONDE_API_KEY"; // TODO replace with your API key
private static final String DONDE_APP_ID = "YOUR_DONDE_DONDE_APP_ID"; // TODO replace with your App ID
...    
Donde.getInstance().init(DONDE_API_KEY, DONDE_APP_ID);
```

We recommend initializing the SDK in the `onCreate` method of your `Application` class:
```
public class MyApplication extends Application {
    private static final String DONDE_API_KEY = "YOUR_DONDE_API_KEY"; // TODO
    private static final String DONDE_APP_ID = "YOUR_DONDE_DONDE_APP_ID"; // TODO

    @Override
    public void onCreate() {
        super.onCreate();
        // Init Donde SDK
        Donde.getInstance().init(DONDE_API_KEY, DONDE_APP_ID);
    }
}
```

And correspondingly in your `AndroidManifest.xml`:
```
<application
  android:name=".MyApplication">
</application>   
```
        
### Donde Visual Search Widget

#### Layout XML
To add the Donde visual search widget to your Android project, add the following snippet to your layout XML file:
```
<co.runloop.donde.DondeSearchView
            android:id="@+id/donde_search_view"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"/>
```

#### Widget Initialization

1. Get a reference to your DondeSearchView:
```
DondeSearchView dondeSearchView = findViewById(R.id.donde_search_view);
```

2. Define a listener for widget events:
```
DondeListener mListener = new DondeListener() {
    @Override
    public void onWidgetLoaded() {
        // Called when the widget is loaded
    }

    @Override
    public void onWidgetLoadFailed(JSONObject error) {
        // Called when the widget failed to load
    }

    @Override
    public void onWidgetHeightSet(int height) {
        // Called when the widget sets its own height
    }

    @Override
    public void onSearchResults(JSONObject results) {
        // Called when the widget return new search results
    }

    @Override
    public void onSearchChanged() {
        // Called when the user interacts with the widget
    }

    @Override
    public void onSearchFailed(String error) {
        // Called when the search failed
    }

    @Override
    public void onEvent(JSONObject event) {
        // Called upon general analytics events
    }

    @Override
    public void onError(JSONObject error) {
        // Called upon general error
    }

    @Override
    public void onItemClick(JSONObject item) {
        // Called when the users clicks an item inside the widget (Similar items view only)
    }
};
```

3. Initialize the widget
```
dondeSearchView.initWidget(mListener)
```

Alternatively you can also specify custom options for initialization:
```
List<String> categoriesWhiteList = new LinkedList<>();
categoriesWhiteList.add("Dress");
categoriesWhiteList.add("Pants");
dondeSearchView.initWidget(mListener, new DondeSearchViewOptions(
  "Dress", // lockCategory
  categoriesWhiteList, // categories
  "Custom question", // categoryQuestionText
  1, // animationShakeStartOffset
  2, // animationDarkenStartOffset
  "Custom user ID", // currentUserId
  false // desktopSortAndFilter
);
```

The options fields are:

- String lockCategory; // Lock the widget on a specific category, without letting the user change it. E.g. "Dress".
- List<String> categories; // White-list categories e.g. ["Dress", "Pants"].
- String categoryQuestionText; // Override main question text.
- int animationShakeStartOffset; // Time period in seconds after which the widget shakes itself to catch the user attention.
- int animationDarkenStartOffset; // Time period in seconds after which the widget darkens the rest of the screen around it to catch the user attention.
- String currentUserId; // Signed-in user ID.
- boolean desktopSortAndFilter; // Display sort and filter bars. Default is false.

#### Widget State Management

Get a the serialized state of the widget:
```
dondeSearchView.getState(value -> {Log.i(TAG, "getState cb value: " + value);});
```

Set a the serialized state of the widget:
```
String widgetSerializedState = "...";
dondeSearchView.setState(widgetSerializedState);
```

Clear the state of the widget:
```
dondeSearchView.clearState();
```

#### Performing Search


##### Simple Search
Perform a search based on the current state of the widget:
```
dondeSearchView.performSearch();
```

##### Search With Parameters
Perform a search based on the current state of the widget with additional parameters:
```
dondeSearchView.performSearch(new DondeSearchParams(
    10.5f, // minPrice
    1000.0f, // maxPrice
    DondeSearchSortType.SORT_ASC, // sortType
    null, // sizes
    null, // brands
    0, // offset
    8, // limit
    null // filterFactors
));
```

The search parameters fields are:
- float minPrice; // Minimum price for search results.
- float maxPrice; // Maximum price for search results.
- DondeSearchSortType sortType; // Sort type: SORT_ASC, SORT_DESC or null.
- List<String> sizes; // Size of the items. Values will be provided by Donde.
- List<String> brands; // Brands of the items. Values will be provided by Donde.
- int offset; //  Skip a given number of results before beginning to return results (e.g. for pagination).
- int limit; // Limit the number of results returned (e.g. for pagination).
- String filterFactors; // A query string encoded object, e.g. "wear_to[]=Festival,Party&popularity[]=5,6&price_ranges[]=0,100,300,400.".
    
Search results are returned via the listener callback `onSearchResults` in `JSONObject` format.
You can check the code of the sample app to see an example of how search results are parsed and displayed.

### Donde Similar Items Widget

#### Layout XML
To add the Donde similar items widget to your Android project, add the following snippet to your layout XML file:
```
<co.runloop.donde.DondeSimilarItemsView
            android:id="@+id/donde_similar_items_view"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />
```

#### Widget Initialization

1. Get a reference to your DondeSimilarItemsView:
```
DondeSimilarItemsView dondeSimilarItemsView = findViewById(R.id.donde_similar_items_view);
```

2. Define a listener for widget events (OPTIONAL):
```
DondeListener mListener = new DondeListener() {
    @Override
    public void onWidgetLoaded() {
        // Called when the widget is loaded
    }

    @Override
    public void onWidgetLoadFailed(JSONObject error) {
        // Called when the widget failed to load
    }

    @Override
    public void onWidgetHeightSet(int height) {
        // Called when the widget sets its own height
    }

    @Override
    public void onSearchResults(JSONObject results) {
        // Called when the widget return new search results
    }

    @Override
    public void onSearchChanged() {
        // Called when the user interacts with the widget
    }

    @Override
    public void onSearchFailed(String error) {
        // Called when the search failed
    }

    @Override
    public void onEvent(JSONObject event) {
        // Called upon general analytics events
    }

    @Override
    public void onError(JSONObject error) {
        // Called upon general error
    }

    @Override
    public void onItemClick(JSONObject item) {
        // Called when the users clicks an item inside the widget (Similar items view only)
    }
};
```

3. Initialize the widget
```
dondeSimilarItemsView.initWidget(mListener, new DondeSimilarItemsViewOptions(
    CUSTOMER_PRODUCT_ID, // itemId
    false, // displaySimilarItems
    new DondeSearchParams(
        10.5f,
        1000.0f,
        DondeSearchSortType.SORT_ASC,
        null,
        null,
        0,
        10,
        null
    ), // searchParams
    true, // displayCurrentDomain
    true, // displayCurrentDomainFeatures
    true, // displayOtherDomains
    true, // displayOtherDomainsFeatures
    true // repeatItems
);
```

The options fields are:

- String itemId; // Current item customer ID. Can be retrieved from search results.
- boolean displaySimilarItems; // Display similar items as part of the widget or not.
- DondeSearchParams searchParams; // Search parameters.
- boolean displayCurrentDomain; // Display items of the same category as the current item. Optional, default true.
- boolean displayCurrentDomainFeatures; // Display features for items of the same category as the current item. Optional, default true.
- boolean displayOtherDomains; // Display items of other categories than the current item's category. Optional, default false.
- boolean displayOtherDomainsFeatures; // Display features for items of other categories than the current item's category. Optional, default false.
- boolean repeatItems; // Enable infinite-scroll for the similar items. Optional, default false.

#### Performing Search

#### Simple Search
Perform a search based on the current state of the widget:
```
dondeSimilarItemsView.performSearch();
```

#### Search With Parameters
Perform a search based on the current state of the widget with additional parameters:
```
dondeSimilarItemsView.performSearch(new DondeSearchParams(
    10.5f, // minPrice
    1000.0f, // maxPrice
    DondeSearchSortType.SORT_ASC, // sortType
    null, // sizes
    null, // brands
    0, // offset
    8, // limit
    null // filterFactors
));
```
Search results are returned via the listener callback `onSearchResults` in `JSONObject` format.
You can check the code of the sample app to see an example of how search results are parsed and displayed.