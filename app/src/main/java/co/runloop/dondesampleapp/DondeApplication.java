package co.runloop.dondesampleapp;

import android.app.Application;
import android.util.Log;

import co.runloop.donde.Donde;

public class DondeApplication extends Application {
    private static final String TAG = DondeApplication.class.getSimpleName();

    private static final String DONDE_API_KEY = null; // TODO ENTER_YOUR_API_KEY
    private static final String DONDE_APP_ID = null; // TODO ENTER_YOUR_APP_ID

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        super.onCreate();
        // Init Donde SDK
        Donde.getInstance().init(getApplicationContext(), DONDE_API_KEY, DONDE_APP_ID);
    }
}

