package co.runloop.dondesampleapp;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;

import co.runloop.donde.DondeSearchParams;
import co.runloop.donde.DondeSearchSortType;

public class DondeSearchParamsDialog extends DialogFragment {
    //private static final String TAG = "DondeSearchParamsDialog";

    private static final String KEY_LAST_USED_SEARCH_PARAMS = "key_last_used_search_params";

    private DondeSearchParamsDialogListener mListener;

    static DondeSearchParamsDialog newInstance(DondeSearchParams searchParams) {
        DondeSearchParamsDialog f = new DondeSearchParamsDialog();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(KEY_LAST_USED_SEARCH_PARAMS, searchParams);
        f.setArguments(args);
        return f;
    }

    private EditText minPriceEditText;
    private EditText maxPriceEditText;
    private EditText offsetEditText;
    private EditText limitEditText;
    private EditText brandsEditText;
    private EditText filterFactorEditText;
    private CheckBox sSizeCheckBox;
    private CheckBox mSizeCheckBox;
    private CheckBox lSizeCheckBox;
    private CheckBox xlSizeCheckBox;
    private TextView sortTextView;
    private ImageButton sortImageButton;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_search_params, null);
        builder.setView(view);

        builder.setTitle(getString(R.string.filter));

        builder.setPositiveButton(getString(R.string.apply), (dialog, id) -> {
            DondeSearchParams searchParams = getDondeSearchParamsFromUI();
            if (searchParams == null) {
                mListener.onClearFilter(DondeSearchParamsDialog.this);

            } else {
                mListener.onApplyFilter(DondeSearchParamsDialog.this, getDondeSearchParamsFromUI());
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), (dialog, id) -> mListener.onClose(DondeSearchParamsDialog.this));

        minPriceEditText = view.findViewById(R.id.min_price_edit_text);
        maxPriceEditText = view.findViewById(R.id.max_price_edit_text);

        sSizeCheckBox = view.findViewById(R.id.s_size_checkbox);
        mSizeCheckBox = view.findViewById(R.id.m_size_checkbox);
        lSizeCheckBox = view.findViewById(R.id.l_size_checkbox);
        xlSizeCheckBox = view.findViewById(R.id.xl_size_checkbox);

        brandsEditText = view.findViewById(R.id.brands_edit_text);
        filterFactorEditText = view.findViewById(R.id.filter_factor_edit_text);
        offsetEditText = view.findViewById(R.id.offset_edit_text);
        limitEditText = view.findViewById(R.id.limit_edit_text);

        sortImageButton = view.findViewById(R.id.sort_image_button);
        sortTextView = view.findViewById(R.id.sort_text_view);
        sortTextView.setOnClickListener(v -> {
            DondeSearchSortType sortType = DondeUtils.getSortTypeFromTag(v.getTag());
            if (sortType == null) {
                v.setTag(DondeSearchSortType.SORT_ASC.name());
                sortImageButton.setImageResource(android.R.drawable.arrow_up_float);
            }
            else if (sortType == DondeSearchSortType.SORT_ASC) {
                v.setTag(DondeSearchSortType.SORT_DESC.name());
                sortImageButton.setImageResource(android.R.drawable.arrow_down_float);
            } else if (sortType == DondeSearchSortType.SORT_DESC) {
                v.setTag("");
                sortImageButton.setImageBitmap(null);
            }
        });

        Button autoFillButton = view.findViewById(R.id.auto_fill_button);
        autoFillButton.setOnClickListener(v -> {
            minPriceEditText.setText(getString(R.string.def_min_price));
            maxPriceEditText.setText(getString(R.string.def_max_price));
            brandsEditText.setText(getString(R.string.def_brands));
            sSizeCheckBox.setChecked(true);
            mSizeCheckBox.setChecked(true);
            lSizeCheckBox.setChecked(true);
            xlSizeCheckBox.setChecked(true);
            offsetEditText.setText(getString(R.string.def_offset));
            limitEditText.setText(getString(R.string.def_limit));
            sortImageButton.setTag(DondeSearchSortType.SORT_ASC.name());
            filterFactorEditText.setText(getString(R.string.def_filter_factor));
        });

        Button clearButton = view.findViewById(R.id.clear_button);
        clearButton.setOnClickListener(v -> {
            minPriceEditText.setText("");
            maxPriceEditText.setText("");
            brandsEditText.setText("");
            sSizeCheckBox.setChecked(false);
            mSizeCheckBox.setChecked(false);
            lSizeCheckBox.setChecked(false);
            xlSizeCheckBox.setChecked(false);
            offsetEditText.setText("");
            limitEditText.setText("");
            sortImageButton.setTag("");
            filterFactorEditText.setText("");
        });

        handleInputArgs(savedInstanceState);
        return builder.create();
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeSearchParamsDialogListener) activity;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                + " must implement DondeSearchParamsDialogListener");
        }
    }


    private List<String> getSizesFromUI() {
        List<String> sizes = new ArrayList<>();
        if (sSizeCheckBox.isChecked()) {
            sizes.add("Small");
        }
        if (mSizeCheckBox.isChecked()) {
            sizes.add("Medium");
        }
        if (lSizeCheckBox.isChecked()) {
            sizes.add("Large");
        }
        if (xlSizeCheckBox.isChecked()) {
            sizes.add("X-Large");
        }

        if (sizes.isEmpty()) {
            return null;
        }
        return sizes;
    }

    private DondeSearchParams getDondeSearchParamsFromUI() {
        Float minPrice = DondeUtils.getFloatOrNullIfEmpty(minPriceEditText);
        Float maxPrice = DondeUtils.getFloatOrNullIfEmpty(maxPriceEditText);
        DondeSearchSortType sortType = DondeUtils.getSortTypeFromTag(sortImageButton.getTag());
        List<String> sizes = getSizesFromUI();
        List<String> brands = DondeUtils.getStringListOrNullIfEmpty(brandsEditText);
        Integer offset = DondeUtils.getIntegerOrNullIfEmpty(offsetEditText);
        Integer limit = DondeUtils.getIntegerOrNullIfEmpty(limitEditText);
        String filterFactor = DondeUtils.getStringOrNullIfEmpty(filterFactorEditText);

        if (minPrice != null || maxPrice != null || sortType != null || sizes != null || brands != null || offset != null || limit != null || filterFactor != null) {
            return new DondeSearchParams(
                minPrice,
                maxPrice,
                sortType,
                sizes,
                brands,
                offset,
                limit,
                filterFactor);
        } else {
            return null;
        }
    }

    @SuppressLint("DefaultLocale")
    private void setDondeSearchParamsInUI(DondeSearchParams searchParams) {
        if (searchParams.minPrice != null) {
            minPriceEditText.setText(String.format("%.2f", searchParams.minPrice));
        }
        if (searchParams.maxPrice != null) {
            maxPriceEditText.setText(String.format("%.2f", searchParams.maxPrice));
        }
        if (searchParams.sizes != null) {
            if (searchParams.sizes.contains("Small")) {
                sSizeCheckBox.setChecked(true);
            }
            if (searchParams.sizes.contains("Medium")) {
                mSizeCheckBox.setChecked(true);
            }
            if (searchParams.sizes.contains("Large")) {
                lSizeCheckBox.setChecked(true);
            }
            if (searchParams.sizes.contains("X-Large")) {
                xlSizeCheckBox.setChecked(true);
            }
        }
        if (searchParams.brands != null) {
            brandsEditText.setText(DondeUtils.getCommaSeparatedStringFromList(searchParams.brands));
        }
        if (searchParams.offset != null) {
            offsetEditText.setText(String.format("%d", searchParams.offset));
        }
        if (searchParams.limit != null) {
            limitEditText.setText(String.format("%d", searchParams.limit));
        }
        if (searchParams.filterFactors != null) {
            filterFactorEditText.setText(searchParams.filterFactors);
        }
    }

    private void handleInputArgs(Bundle savedInstanceState) {
        DondeSearchParams searchParams = null;
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                searchParams = (DondeSearchParams) getArguments().getSerializable(KEY_LAST_USED_SEARCH_PARAMS);
            }
        } else {
            searchParams = (DondeSearchParams) savedInstanceState.getSerializable(KEY_LAST_USED_SEARCH_PARAMS);
        }

        if (searchParams != null) {
            setDondeSearchParamsInUI(searchParams);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAST_USED_SEARCH_PARAMS, getDondeSearchParamsFromUI());
        // should be last
        super.onSaveInstanceState(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeSearchParamsDialogListener) context;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                + " must implement DondeSearchParamsDialogListener");
        }
    }

    @Override
    public void onDestroyView() {
        //stop dialog from being dismissed on rotation, due to a bug with the compatibility library:
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public interface DondeSearchParamsDialogListener {

        void onClearFilter(DondeSearchParamsDialog d);

        void onApplyFilter(DondeSearchParamsDialog d, DondeSearchParams params);

        void onClose(DondeSearchParamsDialog d);
    }
}
