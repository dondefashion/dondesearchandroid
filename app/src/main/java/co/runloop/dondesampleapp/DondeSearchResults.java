package co.runloop.dondesampleapp;

import java.util.List;

class DondeSearchResults {
    public int numTotal;
    public List<MySearchResultItem> pageItems;
    public boolean hasMore;


    public DondeSearchResults(int numTotal,
                              List<MySearchResultItem> pageItems,
                              boolean hasMore) {

        this.numTotal = numTotal;
        this.pageItems = pageItems;
        this.hasMore = hasMore;
    }
}

