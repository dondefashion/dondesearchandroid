package co.runloop.dondesampleapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONObject;

import java.util.LinkedList;

import co.runloop.donde.DondeListener;
import co.runloop.donde.DondeSearchParams;
import co.runloop.donde.DondeSearchView;
import co.runloop.donde.DondeSearchViewOptions;

public class DondeSearchViewActivity extends AppCompatActivity implements
    DondeSearchParamsDialog.DondeSearchParamsDialogListener,
    DondeSearchViewInitDialog.DondeSearchViewInitDialogListener,
    DondeStateManagementDialog.DondeStateManagementDialogListener {
    private static final String TAG = DondeSearchViewActivity.class.getSimpleName();

    private static final int DEF_NUM_ITEMS_PER_PAGE = 8;

    private DondeSearchView dondeSearchView;
    private DondeSearchViewOptions options = null;
    private Button filterButton;
    private LinearLayout pageNavigationLayout;
    private TextView pageTextView;
    private MySearchResultsItemsAdapter searchResultsItemsAdapter;

    private String lastGotState = null;
    private DondeSearchParams searchParams = getDefSearchParams();
    private DondeSearchResults lastSearchResults = null;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donde_search_view);
        setTitle(R.string.title_activity_donde_search_view);

        // Init Donde Search view
        dondeSearchView = findViewById(R.id.donde_search_view);
        dondeSearchView.initWidget(mListener);

        filterButton = findViewById(R.id.filter_button);
        filterButton.setOnClickListener(v -> {
            DondeSearchParamsDialog d = DondeSearchParamsDialog.newInstance(searchParams);
            d.show(getSupportFragmentManager(), "DondeSearchParamsDialog");
        });

        pageNavigationLayout = findViewById(R.id.page_navigation_layout);
        pageTextView = findViewById(R.id.page_text_view);

        Button nextPageButton = findViewById(R.id.next_page_button);
        nextPageButton.setOnClickListener(v -> {
            if (lastSearchResults != null) {
                Log.i(TAG, "offset: " + searchParams.offset);
                Log.i(TAG, "num total: " + lastSearchResults.numTotal);
                if (searchParams.offset < lastSearchResults.numTotal) {
                    searchParams.offset += getDefNumItemsPerPage();
                    Log.i(TAG, "new offset: " + searchParams.offset);
                    dondeSearchView.performSearch(searchParams);
                }
            }
        });

        Button prevPageButton = findViewById(R.id.prev_page_button);
        prevPageButton.setOnClickListener(v -> {
            if (lastSearchResults != null) {
                Log.i(TAG, "offset: " + searchParams.offset);
                Log.i(TAG, "num total: " + lastSearchResults.numTotal);
                int numItemsPerPage = getDefNumItemsPerPage();
                if (searchParams.offset >= numItemsPerPage) {
                    searchParams.offset -= numItemsPerPage;
                    Log.i(TAG, "new offset: " + searchParams.offset);
                    dondeSearchView.performSearch(searchParams);
                }
            }
        });

        recyclerView = findViewById(R.id.recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        // recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        // specify an adapter (see also next example)
        searchResultsItemsAdapter = new MySearchResultsItemsAdapter(this, new LinkedList<>(), R.layout.item_search_view, item -> {
            Intent intent = new Intent(DondeSearchViewActivity.this, DondeSimilarItemsViewActivity.class);
            intent.putExtra(DondeUtils.EXTRA_MY_SEARCH_ITEM, item);
            startActivity(intent);
        });
        recyclerView.setAdapter(searchResultsItemsAdapter);
        //recyclerView.setItemAnimator(null);
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
        dondeSearchView.destroyWidget();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_view_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.initialization) {
            DondeSearchViewInitDialog d = DondeSearchViewInitDialog.newInstance(options);
            d.show(getSupportFragmentManager(), "DondeSearchViewInitDialog");
        } else if (id == R.id.state_management) {
            DondeStateManagementDialog d = DondeStateManagementDialog.newInstance(lastGotState);
            d.show(getSupportFragmentManager(), "DondeStateManagementDialog");
        }
        return super.onOptionsItemSelected(item);
    }

    DondeListener mListener = new DondeListener() {

        @Override
        public void onWidgetLoaded() {
            Log.i(TAG, "onWidgetLoaded");
        }

        @Override
        public void onWidgetLoadFailed(JSONObject error) {
            Log.i(TAG, "onWidgetLoadFailed: " + ((error != null) ? error.toString() : ""));
            String msgText = getString(R.string.widget_load_failed);
            if (error != null) {
                msgText += ": ";
                msgText += error.toString();
            }
            Toast.makeText(DondeSearchViewActivity.this, msgText, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onWidgetHeightSet(int height) {
            Log.i(TAG, "onWidgetHeightSet: " + height);
        }

        @Override
        public void onSearchResults(JSONObject results) {
            Log.i(TAG, "onSearchResults");
            lastSearchResults = DondeUtils.getSearchResultItemsFromJson(results);
            Log.i(TAG, "num items: " + lastSearchResults.pageItems.size());
            runOnUiThread(() -> {
                searchResultsItemsAdapter.updateAllItems(lastSearchResults.pageItems);

                if (lastSearchResults.pageItems.size() > 0) {
                    pageNavigationLayout.setVisibility(View.VISIBLE);
                    int offset = (searchParams == null) ? 0 : ((searchParams.offset == null) ? 0 : searchParams.offset);
                    pageTextView.setText(String.format(getString(R.string.page_info), offset + 1, offset + lastSearchResults.pageItems.size(), lastSearchResults.numTotal));
                } else {
                    pageNavigationLayout.setVisibility(View.GONE);
                }

                if (recyclerView.getLayoutManager() != null) {
                    recyclerView.getLayoutManager().scrollToPosition(0);
                }
            });
        }

        @Override
        public void onSearchChanged() {
            Log.i(TAG, "onSearchChanged");
            runOnUiThread(() -> {
                searchParams.offset = 0;
                dondeSearchView.performSearch(searchParams);
            });
        }

        @Override
        public void onSearchFailed(String error) {
            Log.i(TAG, "onSearchFailed: " + error);
            String msgText = getString(R.string.search_failed);
            if (error != null) {
                msgText += ": ";
                msgText += error;
            }
            Toast.makeText(DondeSearchViewActivity.this, msgText, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEvent(JSONObject event) {
            Log.i(TAG, "onEvent: " + ((event != null) ? event.toString() : ""));
        }

        @Override
        public void onError(JSONObject error) {
            Log.i(TAG, "onError: " + ((error != null) ? error.toString() : ""));
            String msgText = getString(R.string.error);
            if (error != null) {
                msgText += ": ";
                msgText += error.toString();
            }
            Toast.makeText(DondeSearchViewActivity.this, msgText, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onItemClick(JSONObject item) {
            Log.i(TAG, "onItemClick: " + ((item != null) ? item.toString() : ""));
        }
    };

    @Override
    public void onClearState(DondeStateManagementDialog d) {
        dondeSearchView.clearState();
    }

    @Override
    public void onSetState(DondeStateManagementDialog d, String state) {
        if (state != null && !state.isEmpty()) {
            dondeSearchView.setState(state);
        }
    }

    @Override
    public void onGetState(DondeStateManagementDialog d) {
        dondeSearchView.getState(state -> {
            lastGotState = state;
            d.onGetStateCb(state);
        });
    }

    @Override
    public void onClose(DondeStateManagementDialog d) {

    }

    @Override
    public void onInit(DondeSearchViewInitDialog d) {
        options = null;
        dondeSearchView.initWidget(mListener);

    }

    @Override
    public void onInitWithOptions(DondeSearchViewInitDialog d, DondeSearchViewOptions options) {
        this.options = options;
        dondeSearchView.initWidget(mListener, options);
    }

    @Override
    public void onClose(DondeSearchViewInitDialog d) {

    }

    @Override
    public void onClearFilter(DondeSearchParamsDialog d) {
        searchParams = getDefSearchParams();
        filterButton.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        dondeSearchView.performSearch(searchParams);
    }

    @Override
    public void onApplyFilter(DondeSearchParamsDialog d, DondeSearchParams params) {
        searchParams = params;
        filterButton.setTextColor(ContextCompat.getColor(this, R.color.colorRed));
        dondeSearchView.performSearch(params);
    }

    @Override
    public void onClose(DondeSearchParamsDialog d) {

    }

    private DondeSearchParams getDefSearchParams() {
        return new DondeSearchParams(null, null, null, null, null, 0, DEF_NUM_ITEMS_PER_PAGE, null);
    }

    private int getDefNumItemsPerPage() {
        return (searchParams.limit == null) ? DEF_NUM_ITEMS_PER_PAGE : searchParams.limit;
    }
}