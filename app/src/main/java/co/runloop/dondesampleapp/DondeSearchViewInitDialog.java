package co.runloop.dondesampleapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.List;

import co.runloop.donde.DondeSearchViewOptions;

public class DondeSearchViewInitDialog extends DialogFragment {
    //private static final String TAG = "DondeSearchViewInitDialog";

    private static final String KEY_OPTIONS = "key_options";

    private DondeSearchViewInitDialogListener mListener;

    static DondeSearchViewInitDialog newInstance(DondeSearchViewOptions options) {
        DondeSearchViewInitDialog f = new DondeSearchViewInitDialog();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(KEY_OPTIONS, options);
        f.setArguments(args);
        return f;
    }

    private DondeSearchViewOptions options;
    private EditText lockCategoryEditText;
    private EditText categoriesWhiteListEditText;
    private EditText customQuestionTextEditText;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_search_view_init, null);
        builder.setView(view);

        builder.setTitle(getString(R.string.initialization));

        builder.setPositiveButton(getString(R.string.init), (dialog, id) -> {
            DondeSearchViewOptions dondeSearchViewOptions = getDondeSimilarItemsViewOptionsFromUI();
            if (dondeSearchViewOptions == null) {
                mListener.onInit(DondeSearchViewInitDialog.this);
            } else {
                mListener.onInitWithOptions(DondeSearchViewInitDialog.this, dondeSearchViewOptions);
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), (dialog, id) -> mListener.onClose(DondeSearchViewInitDialog.this));

        lockCategoryEditText = view.findViewById(R.id.lock_category_edit_text);
        categoriesWhiteListEditText = view.findViewById(R.id.categories_white_list_edit_text);
        customQuestionTextEditText = view.findViewById(R.id.custom_question_text_edit_text);

        Button autoFillButton = view.findViewById(R.id.auto_fill_button);
        autoFillButton.setOnClickListener(v -> {
            lockCategoryEditText.setText(getString(R.string.def_lock_category));
            categoriesWhiteListEditText.setText(getString(R.string.def_categories_white_list));
            customQuestionTextEditText.setText(getString(R.string.def_custom_question_text));
        });

        Button clearButton = view.findViewById(R.id.clear_button);
        clearButton.setOnClickListener(v -> {
            lockCategoryEditText.setText("");
            categoriesWhiteListEditText.setText("");
            customQuestionTextEditText.setText("");
        });

        handleInputArgs(savedInstanceState);
        return builder.create();
    }

    private void handleInputArgs(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                options = (DondeSearchViewOptions)getArguments().getSerializable(KEY_OPTIONS);
            }
        } else {
            options = (DondeSearchViewOptions)savedInstanceState.getSerializable(KEY_OPTIONS);
        }

        if (options != null) {
            setDondeSimilarItemsViewOptionsInUI(options);
        }
    }

    private DondeSearchViewOptions getDondeSimilarItemsViewOptionsFromUI() {
        String lockCategory = DondeUtils.getStringOrNullIfEmpty(lockCategoryEditText);
        List<String> whiteListCategories = DondeUtils.getStringListOrNullIfEmpty(categoriesWhiteListEditText);
        String customQuestionText = DondeUtils.getStringOrNullIfEmpty(customQuestionTextEditText);

        if (lockCategory != null || whiteListCategories != null || customQuestionText != null) {
            return new DondeSearchViewOptions(
                lockCategory,
                whiteListCategories,
                customQuestionText,
                1,
                2,
                "Custom user ID",
                true
            );
        } else {
            return null;
        }
    }

    private void setDondeSimilarItemsViewOptionsInUI(DondeSearchViewOptions options) {
        if (options.lockCategory != null) {
            lockCategoryEditText.setText(options.lockCategory);
        }
        if (options.categories != null) {
            categoriesWhiteListEditText.setText(DondeUtils.getCommaSeparatedStringFromList(options.categories));
        }
        if (options.categoryQuestionText != null) {
            customQuestionTextEditText.setText(options.categoryQuestionText);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_OPTIONS, getDondeSimilarItemsViewOptionsFromUI());
        // should be last
        super.onSaveInstanceState(savedInstanceState);
    }
    
    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeSearchViewInitDialogListener) activity;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                + " must implement DondeSearchViewInitDialogListener");
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeSearchViewInitDialogListener) context;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                + " must implement DondeSearchViewInitDialogListener");
        }
    }

    @Override
    public void onDestroyView() {
        //stop dialog from being dismissed on rotation, due to a bug with the compatibility library:
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public interface DondeSearchViewInitDialogListener {

        void onInit(DondeSearchViewInitDialog d);

        void onInitWithOptions(DondeSearchViewInitDialog d, DondeSearchViewOptions options);

        void onClose(DondeSearchViewInitDialog d);
    }
}
