package co.runloop.dondesampleapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONObject;

import java.util.LinkedList;

import co.runloop.donde.DondeListener;
import co.runloop.donde.DondeSearchParams;
import co.runloop.donde.DondeSimilarItemsView;
import co.runloop.donde.DondeSimilarItemsViewOptions;

public class DondeSimilarItemsViewActivity extends AppCompatActivity implements
    DondeSearchParamsDialog.DondeSearchParamsDialogListener,
    DondeSimilarItemsViewInitDialog.DondeSearchViewInitDialogListener {
    private static final String TAG = DondeSimilarItemsViewActivity.class.getSimpleName();

    private MySearchResultItem currentItem;

    private DondeSimilarItemsView dondeSimilarItemsView;
    private DondeSimilarItemsViewOptions options = null;
    private Button filterButton;
    private RecyclerView recyclerView;
    private MySearchResultsItemsAdapter searchResultsItemsAdapter;

    private DondeSearchParams searchParams = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donde_similar_items_view);
        setTitle(R.string.title_activity_donde_similar_items);

        Intent inputIntent = getIntent();
        if (inputIntent != null) {
            currentItem = (MySearchResultItem) inputIntent.getSerializableExtra(DondeUtils.EXTRA_MY_SEARCH_ITEM);
            if (currentItem != null) {
                options = new DondeSimilarItemsViewOptions(currentItem.getCustomerProductId(), false, null, true, true, false, false, false);
                ImageView imageView = findViewById(R.id.item_image_view);
                new DownloadImageTask(imageView).execute(currentItem.getImageUrl());

                if (currentItem.getUrl() != null) {
                    imageView.setOnClickListener(v -> {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(currentItem.getUrl()));
                        startActivity(browserIntent);
                    });
                }
                // Init Donde Similar Items view
                dondeSimilarItemsView = findViewById(R.id.donde_similar_items_view);
                dondeSimilarItemsView.initWidget(mListener, options);
            }
        }

        filterButton = findViewById(R.id.filter_button);
        filterButton.setOnClickListener(v -> {
            DondeSearchParamsDialog d = DondeSearchParamsDialog.newInstance(searchParams);
            d.show(getSupportFragmentManager(), "DondeSearchParamsDialog");
        });

        recyclerView = findViewById(R.id.recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        // specify an adapter (see also next example)
        searchResultsItemsAdapter = new MySearchResultsItemsAdapter(this, new LinkedList<>(), R.layout.item_similar_items_view, item -> {
            Intent intent = new Intent(DondeSimilarItemsViewActivity.this, DondeSimilarItemsViewActivity.class);
            intent.putExtra(DondeUtils.EXTRA_MY_SEARCH_ITEM, item);
            startActivity(intent);
        });
        recyclerView.setAdapter(searchResultsItemsAdapter);
        // recyclerView.setItemAnimator(null);
    }

    DondeListener mListener = new DondeListener() {

        @Override
        public void onWidgetLoaded() {
            Log.i(TAG, "onWidgetLoaded");
        }

        @Override
        public void onWidgetLoadFailed(JSONObject error) {
            Log.i(TAG, "onWidgetLoadFailed: " + ((error != null) ? error.toString() : ""));
            String msgText = getString(R.string.widget_load_failed);
            if (error != null) {
                Log.i(TAG, error.toString());
                msgText += ": ";
                msgText += error.toString();
            }
            Toast.makeText(DondeSimilarItemsViewActivity.this, msgText, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onWidgetHeightSet(int height) {
            Log.i(TAG, "onWidgetHeightSet: " + height);
        }

        @Override
        public void onSearchResults(JSONObject results) {
            Log.i(TAG, "onSearchResults");
            DondeSearchResults searchResults = DondeUtils.getSearchResultItemsFromJson(results);
            runOnUiThread(() -> {
                searchResultsItemsAdapter.updateAllItems(searchResults.pageItems);
                if (recyclerView.getLayoutManager() != null) {
                    recyclerView.getLayoutManager().scrollToPosition(0);
                }
            });
        }


        @Override
        public void onSearchChanged() {
            Log.i(TAG, "onSearchChanged");
            runOnUiThread(() -> {
                if (searchParams == null) {
                    dondeSimilarItemsView.performSearch();
                } else {
                    searchParams.offset = 0;
                    dondeSimilarItemsView.performSearch(searchParams);
                }
            });
        }

        @Override
        public void onSearchFailed(String error) {
            Log.i(TAG, "onSearchFailed: " + error);
            String msgText = getString(R.string.search_failed);
            if (error != null) {
                msgText += ": ";
                msgText += error;
            }
            Toast.makeText(DondeSimilarItemsViewActivity.this, msgText, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onEvent(JSONObject event) {
            Log.i(TAG, "onEvent: " + ((event != null) ? event.toString() : ""));
        }

        @Override
        public void onError(JSONObject error) {
            Log.i(TAG, "onError");
            String msgText = getString(R.string.error);
            if (error != null) {
                msgText += ": ";
                msgText += error.toString();
            }
            Toast.makeText(DondeSimilarItemsViewActivity.this, msgText, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onItemClick(JSONObject item) {
            Log.i(TAG, "onItemClick: " + ((item != null) ? item.toString() : ""));
        }
    };

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
        dondeSimilarItemsView.destroyWidget();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.similar_items_view_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.initialization) {
            if (currentItem != null) {
                DondeSimilarItemsViewInitDialog d = DondeSimilarItemsViewInitDialog.newInstance(options);
                d.show(getSupportFragmentManager(), "DondeSimilarItemsViewInitDialog");
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInitWithOptions(DondeSimilarItemsViewInitDialog d, DondeSimilarItemsViewOptions options) {
        this.options = options;
        dondeSimilarItemsView.initWidget(mListener, options);
        if (options.displaySimilarItems) {
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClose(DondeSimilarItemsViewInitDialog d) {

    }

    @Override
    public void onClearFilter(DondeSearchParamsDialog d) {
        searchParams = null;
        filterButton.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        dondeSimilarItemsView.performSearch();
    }

    @Override
    public void onApplyFilter(DondeSearchParamsDialog d, DondeSearchParams params) {
        searchParams = params;
        filterButton.setTextColor(ContextCompat.getColor(this, R.color.colorRed));
        dondeSimilarItemsView.performSearch(params);
    }

    @Override
    public void onClose(DondeSearchParamsDialog d) {

    }
}
