package co.runloop.dondesampleapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import co.runloop.donde.DondeSimilarItemsViewOptions;

public class DondeSimilarItemsViewInitDialog extends DialogFragment {
    //private static final String TAG = "DondeSimilarItemsViewInitDialog";

    private static final String KEY_OPTIONS = "key_options";

    private DondeSearchViewInitDialogListener mListener;

    static DondeSimilarItemsViewInitDialog newInstance(DondeSimilarItemsViewOptions options) {
        DondeSimilarItemsViewInitDialog f = new DondeSimilarItemsViewInitDialog();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(KEY_OPTIONS, options);
        f.setArguments(args);
        return f;
    }

    private DondeSimilarItemsViewOptions options;
    private Switch displaySimilarItemsSwitch;
    private Switch displayCurrentDomainSwitch;
    private Switch displayCurrentDomainFeaturesSwitch;
    private Switch displayOtherDomainsSwitch;
    private Switch displayOtherDomainsFeaturesSwitch;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_similar_items_view_init, null);
        builder.setView(view);

        builder.setTitle(getString(R.string.initialization));

        builder.setPositiveButton(getString(R.string.init), (dialog, id) -> mListener.onInitWithOptions(DondeSimilarItemsViewInitDialog.this, getDondeSimilarItemsViewOptionsFromUI()));

        builder.setNegativeButton(getString(R.string.cancel), (dialog, id) -> mListener.onClose(DondeSimilarItemsViewInitDialog.this));

        displaySimilarItemsSwitch = view.findViewById(R.id.display_similar_items_switch);
        displayCurrentDomainSwitch = view.findViewById(R.id.display_current_domain_switch);
        displayCurrentDomainFeaturesSwitch = view.findViewById(R.id.display_current_domain_features_switch);
        displayOtherDomainsSwitch = view.findViewById(R.id.display_other_domain_switch);
        displayOtherDomainsFeaturesSwitch = view.findViewById(R.id.display_other_domain_features_switch);

        handleInputArgs(savedInstanceState);
        return builder.create();
    }

    private void handleInputArgs(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                options = (DondeSimilarItemsViewOptions)getArguments().getSerializable(KEY_OPTIONS);
            }
        } else {
            options = (DondeSimilarItemsViewOptions)savedInstanceState.getSerializable(KEY_OPTIONS);
        }

        if (options != null) {
            setDondeSimilarItemsViewOptionsInUI(options);
        }
    }

    private DondeSimilarItemsViewOptions getDondeSimilarItemsViewOptionsFromUI() {
        return new DondeSimilarItemsViewOptions(
            options.itemId,
            displaySimilarItemsSwitch.isChecked(),
            null,
            displayCurrentDomainSwitch.isChecked(),
            displayCurrentDomainFeaturesSwitch.isChecked(),
            displayOtherDomainsSwitch.isChecked(),
            displayOtherDomainsFeaturesSwitch.isChecked(),
            true);
    }

    private void setDondeSimilarItemsViewOptionsInUI(DondeSimilarItemsViewOptions options) {
        displaySimilarItemsSwitch.setChecked(options.displaySimilarItems);
        displayCurrentDomainSwitch.setChecked(options.displayCurrentDomain);
        displayCurrentDomainFeaturesSwitch.setChecked(options.displayCurrentDomainFeatures);
        displayOtherDomainsSwitch.setChecked(options.displayOtherDomains);
        displayOtherDomainsFeaturesSwitch.setChecked(options.displayOtherDomainsFeatures);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_OPTIONS, getDondeSimilarItemsViewOptionsFromUI());
        // should be last
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeSearchViewInitDialogListener) activity;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                + " must implement DondeSearchViewInitDialogListener");
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeSearchViewInitDialogListener) context;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                + " must implement DondeSearchViewInitDialogListener");
        }
    }

    @Override
    public void onDestroyView() {
        //stop dialog from being dismissed on rotation, due to a bug with the compatibility library:
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public interface DondeSearchViewInitDialogListener {

        void onInitWithOptions(DondeSimilarItemsViewInitDialog d, DondeSimilarItemsViewOptions options);

        void onClose(DondeSimilarItemsViewInitDialog d);
    }
}
