package co.runloop.dondesampleapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class DondeStateManagementDialog extends DialogFragment {
    //private static final String TAG = "EditTextDialogFragmentSupport";

    private static final String KEY_LAST_GOT_STATE = "key_last_got_state";

    private DondeStateManagementDialogListener mListener; // recreated from activity

    // Views
    private TextView lastGotStateTextView;

    static DondeStateManagementDialog newInstance(String lastGotState) {
        DondeStateManagementDialog f = new DondeStateManagementDialog();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString(KEY_LAST_GOT_STATE, lastGotState);
        f.setArguments(args);
        return f;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_state_management, null);
        builder.setView(view);

        builder.setTitle(getString(R.string.state_management));

        Button getStateButton = view.findViewById(R.id.get_state_button);
        getStateButton.setOnClickListener(v -> mListener.onGetState(DondeStateManagementDialog.this));

        Button setStateButton = view.findViewById(R.id.set_state_button);
        setStateButton.setOnClickListener(v -> mListener.onSetState(DondeStateManagementDialog.this,lastGotStateTextView.getText().toString()));

        Button clearStateButton = view.findViewById(R.id.clear_state_button);
        clearStateButton.setOnClickListener(v -> mListener.onClearState(DondeStateManagementDialog.this));

        lastGotStateTextView = view.findViewById(R.id.last_saved_state_text_view);
        lastGotStateTextView.setOnClickListener(v -> {
            Activity activity = getActivity();
            if (activity != null) {
                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard != null) {
                    ClipData clip = ClipData.newPlainText(getString(R.string.last_got_state), lastGotStateTextView.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(activity,getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();
                }
            }
        });
        handleInputArgs(savedInstanceState);
        return builder.create();
    }

    private void handleInputArgs(Bundle savedInstanceState) {
        String text = null;

        if (savedInstanceState == null) {
            if (getArguments() != null) {
                text = getArguments().getString(KEY_LAST_GOT_STATE);
            }
        } else {
            text = savedInstanceState.getString(KEY_LAST_GOT_STATE, null);
        }

        if (text != null) {
            lastGotStateTextView.setText(text);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putString(KEY_LAST_GOT_STATE, lastGotStateTextView.getText().toString());
        // should be last
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeStateManagementDialogListener) activity;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                + " must implement DondeStateManagementDialogListener");
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DondeStateManagementDialogListener) context;
        }
        catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                + " must implement DondeStateManagementDialogListener");
        }
    }

    @Override
    public void onDestroyView() {
        //stop dialog from being dismissed on rotation, due to a bug with the compatibility library:
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    void onGetStateCb(String state) {
        lastGotStateTextView.setText(state);
    }

    public interface DondeStateManagementDialogListener {
        void onClearState(DondeStateManagementDialog d);

        void onSetState(DondeStateManagementDialog d, String state);

        void onGetState(DondeStateManagementDialog d);

        void onClose(DondeStateManagementDialog d);
    }
}
