package co.runloop.dondesampleapp;

import android.util.Log;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import co.runloop.donde.DondeSearchSortType;

class DondeUtils {
    private static final String TAG = DondeUtils.class.getSimpleName();

    static final String EXTRA_MY_SEARCH_ITEM = "extra_my_search_item";


    static DondeSearchResults getSearchResultItemsFromJson(JSONObject jsonObject) {
        List<MySearchResultItem> list = new LinkedList<>();
        try {
            int numTotal = jsonObject.getInt("total");
            Log.i(TAG, "num total: " + numTotal);
            JSONArray resultsArray = jsonObject.getJSONArray("results");
            Log.i(TAG, "num results: " + resultsArray.length());
            for (int i = 0; i < resultsArray.length(); i++) {
                JSONObject curItem = (JSONObject) resultsArray.get(i);

                ArrayList<String> sizes = new ArrayList<>();
                JSONArray jArray = curItem.getJSONArray("sizes");
                if (jArray != null) {
                    for (int j = 0; j < jArray.length(); j++) {
                        sizes.add(jArray.getString(j));
                    }
                }

                list.add(new MySearchResultItem(
                    curItem.getString("_id"),
                    curItem.getString("customer_product_id"),
                    curItem.getJSONObject("retailer").getString("_id"),
                    curItem.getString("title"),
                    curItem.getJSONObject("custom_data").getString("Price"),
                    curItem.getJSONObject("custom_data").getString("Sale Price"),
                    curItem.getJSONObject("custom_data").getString("Currency_Format"),
                    curItem.getJSONObject("custom_data").getString("Largeimage"),
                    sizes,
                    curItem.getString("url")
                ));
            }
            boolean hasMore = jsonObject.getBoolean("has_more");
            Log.i(TAG, "has more: " + hasMore);
            return new DondeSearchResults(numTotal, list, hasMore);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return new DondeSearchResults(0, list, false);
    }

    static String getStringOrNullIfEmpty(EditText editText) {
        String str = editText.getText().toString();
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    static List<String> getStringListOrNullIfEmpty(EditText editText) {
        String str = editText.getText().toString();
        if (str.isEmpty()) {
            return null;
        }
        return Arrays.asList(str.split(","));
    }

    static Float getFloatOrNullIfEmpty(EditText editText) {
        try {
            return Float.valueOf(editText.getText().toString());
        }
        catch (Exception e) {
            return null;
        }
    }

    static Integer getIntegerOrNullIfEmpty(EditText editText) {
        try {
            return Integer.valueOf(editText.getText().toString());
        }
        catch (Exception e) {
            return null;
        }
    }

    static String getCommaSeparatedStringFromList(List<String> list) {
        if (list.size() > 0) {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < list.size() - 1; i++) {
                str.append(list.get(i)).append(",");
            }
            str.append(list.get(list.size() - 1));
            return str.toString();
        }
        return "";
    }

    static DondeSearchSortType getSortTypeFromTag(Object tag) {
        if (tag instanceof String) {
            String tagStr = (String) tag;
            if (tagStr.equals(DondeSearchSortType.SORT_ASC.name())) {
                return DondeSearchSortType.SORT_ASC;
            } else if (tagStr.equals(DondeSearchSortType.SORT_DESC.name())) {
                return DondeSearchSortType.SORT_DESC;
            }
        }
        return null;
    }
}

