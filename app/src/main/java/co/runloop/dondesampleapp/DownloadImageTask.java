package co.runloop.dondesampleapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private static final String TAG = DownloadImageTask.class.getSimpleName();

    private ImageView imageView;

    DownloadImageTask(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.imageView.setImageBitmap(null);
    }

    protected Bitmap doInBackground(String... urls) {
        Log.i(TAG, "doInBackground");
        String url = urls[0];
        Log.i(TAG, "image URL: " + url);
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        Log.i(TAG, "onPostExecute");

        if (result != null) {
            Log.i(TAG, "valid result");
        } else {
            Log.w(TAG, "null result");
        }
        imageView.setImageBitmap(result);
    }
}
