package co.runloop.dondesampleapp;

import java.io.Serializable;
import java.util.List;

class MySearchResultItem implements Serializable {
    private String id;
    private String customerProductId;
    private String retailerId;
    private String title;
    private String price;
    private String salePrice;
    private String currency;
    private String imageUrl;
    private List<String> sizes;
    private String url;

    MySearchResultItem(String id, String customerProductId, String retailerId, String title, String price, String salePrice, String currency, String imageUrl, List<String> sizes, String url) {
        this.id = id;
        this.customerProductId = customerProductId;
        this.retailerId = retailerId;
        this.title = title;
        this.price = price;
        this.salePrice = salePrice;
        this.currency = currency;
        this.imageUrl = imageUrl;
        this.sizes = sizes;
        this.url = url;
    }

    String getId() {
        return id;
    }

    String getCustomerProductId() {
        return customerProductId;
    }

    String getRetailerId() {
        return retailerId;
    }

    String getTitle() {
        return title;
    }

    String getPrice() {
        return price;
    }

    String getSalePrice() {
        return salePrice;
    }

    String getCurrency() {
        return currency;
    }

    String getImageUrl() {
        return imageUrl;
    }

    List<String> getSizes() {
        return sizes;
    }

    String getUrl() {
        return url;
    }
}
