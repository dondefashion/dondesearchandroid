package co.runloop.dondesampleapp;

interface MySearchResultListListener {
    void onItemClicked(MySearchResultItem item);
}