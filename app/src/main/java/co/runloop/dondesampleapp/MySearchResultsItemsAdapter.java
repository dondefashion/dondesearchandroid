package co.runloop.dondesampleapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MySearchResultsItemsAdapter extends RecyclerView.Adapter<MySearchResultsItemsAdapter.MyViewHolder> {
//    private static final String TAG = MySearchResultsItemsAdapter.class.getSimpleName();

    private List<MySearchResultItem> dataSet;
    private final MySearchResultListListener listener;
    private final Context context;
    private final int layoutId;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        final ImageView imageView;
        final TextView titleTextView;
        final TextView priceTextView;
        final TextView salePriceTextView;
        final TextView sizesTextView;

        MyViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.item_image_view);
            titleTextView = v.findViewById(R.id.item_title_text_view);
            priceTextView = v.findViewById(R.id.item_price_text_view);
            salePriceTextView = v.findViewById(R.id.item_sale_price_text_view);
            sizesTextView = v.findViewById(R.id.item_sizes_text_view);
            v.setOnClickListener(v1 -> {
                if (listener != null) {
                    listener.onItemClicked(dataSet.get(getAdapterPosition()));
                }
            });
        }
    }

    // Provide a suitable constructor (depends on the kind of data set)
    MySearchResultsItemsAdapter(Context context, List<MySearchResultItem> dataSet, int layoutId, MySearchResultListListener listener) {
        this.context = context;
        this.dataSet = dataSet;
        this.layoutId = layoutId;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layoutId, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your data set at this position
        // - replace the contents of the view with that element
        final MySearchResultItem item = dataSet.get(position);

        holder.titleTextView.setText(item.getTitle());
        if (layoutId == R.layout.item_search_view) {
            String priceStr = context.getString(R.string.price) + ": " + item.getCurrency() + item.getPrice();
            holder.priceTextView.setText(priceStr);
            String salePriceStr = context.getString(R.string.sale_price) + ": " + item.getCurrency() + item.getSalePrice();
            holder.salePriceTextView.setText(salePriceStr);
            String sizesStr = context.getString(R.string.sizes) + ": " + DondeUtils.getCommaSeparatedStringFromList(item.getSizes());
            holder.sizesTextView.setText(sizesStr);
        } else {
            holder.titleTextView.setText(item.getTitle());
            String priceStr = item.getCurrency() + item.getPrice();
            holder.priceTextView.setText(priceStr);
        }
        new DownloadImageTask(holder.imageView).execute(item.getImageUrl());
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    void updateAllItems(List<MySearchResultItem> newItems) {
        dataSet = newItems;
        notifyDataSetChanged();
    }
}
